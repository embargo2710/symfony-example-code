<?php

namespace App\Controller\Admin;

use App\Entity\Recipe;
use App\Entity\Spirit;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class FavoriteController
 * @package App\Controller\Api
 */
class FavoriteController extends Controller
{
    /**
     * @Route("/api/user/favorite-spirit/{id}")
     * @Method({"PUT"})
     *
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function favoriteSpirit(
        UserInterface $user,
        EntityManagerInterface $entityManager,
        Request $request
    ) {
        $id = (int)$request->attributes->get('id');

        if (!$id) {
            return new JsonResponse(['message' => 'Spirit ID not found'], 404);
        }

        $spirit = $entityManager->find(Spirit::class, $id);

        if (!$spirit) {
            return new JsonResponse(['message' => 'Spirit not found'], 404);
        }

        $user->addSpirit($spirit);

        $entityManager->persist($user);

        $entityManager->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Route("/api/user/unfavorite-spirit/{id}")
     * @Method({"PUT"})
     *
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return JsonResponse
     */
    public function unfavoriteSpirit(
        UserInterface $user,
        EntityManagerInterface $entityManager,
        Request $request
    ) {
        $id = (int)$request->attributes->get('id');

        if (!$id) {
            return new JsonResponse(['message' => 'Spirit ID not found'], 404);
        }

        $spirit = $entityManager->find(Spirit::class, $id);

        $user->removeSpirit($spirit);

        $entityManager->persist($user);

        $entityManager->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Route("/api/user/favorite-recipe/{id}")
     * @Method({"PUT"})
     *
     * @param User $user
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function favoriteRecipe(
        UserInterface $user,
        EntityManagerInterface $entityManager,
        Request $request
    ) {
        $id = (int)$request->attributes->get('id');

        if (!$id) {
            return new JsonResponse(['message' => 'Recipe ID not found'], 404);
        }

        $recipe = $entityManager->find(Recipe::class, $id);

        if (!$recipe) {
            return new JsonResponse(['message' => 'Recipe not found'], 404);
        }

        $user->addRecipe($recipe);

        $entityManager->persist($user);

        $entityManager->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }

    /**
     * @Route("/api/user/unfavorite-recipe/{id}")
     * @Method({"PUT"})
     *
     * @param User $user
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function unfavoriteRecipe(
        UserInterface $user,
        EntityManagerInterface $entityManager,
        Request $request
    ) {
        $id = (int)$request->attributes->get('id');

        if (!$id) {
            return new JsonResponse(['message' => 'Recipe ID not found'], 404);
        }

        $recipe = $entityManager->find(Recipe::class, $id);

        if (!$recipe) {
            return new JsonResponse(['message' => 'Recipe not found'], 404);
        }

        $user->removeRecipe($recipe);

        $entityManager->persist($user);

        $entityManager->flush();

        return new JsonResponse([
            'result' => true,
        ]);
    }
}
