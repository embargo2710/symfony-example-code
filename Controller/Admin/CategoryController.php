<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories", name="categories_")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/", name="index")
     *
     * @param CategoryRepository $repository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CategoryRepository $repository)
    {
        return $this->render('admin/categories/index.html.twig', [
            'data' => $repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $model = new Category();

        $form = $this->createForm(CategoryType::class, $model);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($form->getData());
            $entityManager->flush();

            $this->addFlash(
                'success',
                'The new Category is successfully created!'
            );

            return $this->redirectToRoute('categories_index');
        }

        return $this->render('admin/categories/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Request $request
     * @param CategoryRepository $repository
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, CategoryRepository $repository, EntityManagerInterface $entityManager)
    {
        $model = $repository->find($request->attributes->get('id'));

        if (!$model) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(CategoryType::class, $model);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($form->getData());
            $entityManager->flush();

            $this->addFlash(
                'success',
                'The Category is successfully updated!'
            );

            return $this->redirectToRoute('categories_index');
        }

        return $this->render('admin/categories/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     *
     */
    /**
     * @Route("/delete/{id}", name="delete")
     *
     * @param $id
     * @param CategoryRepository $repository
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function delete($id, CategoryRepository $repository, EntityManagerInterface $entityManager)
    {
        $model = $repository->find($id);

        if (!$model) {
            throw $this->createNotFoundException();
        }

        try {
            $entityManager->remove($model);
            $entityManager->flush();
            $this->addFlash('success', 'The Category is successfully deleted!');
        } catch (ForeignKeyConstraintViolationException $exception) {
            $this->addFlash('danger', 'It is impossible to remove the related entity!');
        }

        return $this->redirectToRoute('categories_index');
    }
}
