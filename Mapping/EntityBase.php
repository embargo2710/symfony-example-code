<?php

namespace App\Mapping;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class EntityBase
 * @ORM\HasLifecycleCallbacks
 */
class EntityBase implements EntityBaseInterface
{
    const STATUS_INACTIVE = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @var int $status
     *
     * @ORM\Column(name="status", type="smallint", nullable=true)
     */
    protected $status;

    /**
     * @Groups("read")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @Groups("read")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');

        $this->setUpdatedAt($dateTimeNow);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }

        if (!$this->getStatus()) {
            $this->setStatus(self::STATUS_ACTIVE);
        }
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt(DateTime $createdAt): EntityBaseInterface
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @inheritdoc
     */
    public function setStatus(int $status): EntityBaseInterface
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt(DateTime $updatedAt): EntityBaseInterface
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}