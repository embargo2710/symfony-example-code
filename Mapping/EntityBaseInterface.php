<?php

namespace App\Mapping;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * EntityBase Interface
 */
interface EntityBaseInterface
{
    /**
     * @return int|null
     */
    public function getStatus(): ?int;

    /**
     * @param int $status
     *
     * @return EntityBaseInterface
     */
    public function setStatus(int $status): self;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void;

    /**
     * Get createdAt
     *
     * @return null|DateTime
     */
    public function getCreatedAt(): ?DateTime;

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self;

    /**
     * Get updatedAt
     *
     * @return Datetime
     */
    public function getUpdatedAt(): ?DateTime;

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): self;
}
