<?php

namespace App\Command;

use App\Entity\News;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

final class NewsLoaderCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var NewsRepository
     */
    private $repository;

    /**
     * @var FilterManager
     */
    private $filterManager;

    /**
     * @var DataManager
     */
    private $dataManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    public function __construct(
        EntityManagerInterface $entityManager,
        NewsRepository $repository,
        FilterManager $filterManager,
        DataManager $dataManager,
        Filesystem $filesystem,
        UploaderHelper $uploaderHelper
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->filterManager = $filterManager;
        $this->dataManager = $dataManager;
        $this->filesystem = $filesystem;
        $this->uploaderHelper = $uploaderHelper;
    }

    protected function configure()
    {
        $this->setName('news-loader');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->getUrls() as $type => $endpoint) {
            $xml = simplexml_load_file($endpoint['rss'], 'SimpleXMLElement', LIBXML_NOCDATA);
            if ($xml->channel && $xml->channel->item) {
                foreach ($xml->channel->item as $item) {
                    try {
                        $news = $this->composeNews($item, $type);
                        $this->entityManager->persist($news);

                        if ($news->getImageFile()) {
                            $binary = $this->dataManager->find(
                                'news',
                                $this->uploaderHelper->asset($news, 'imageFile')
                            );
                            $binary = $this->filterManager->applyFilter($binary, 'news');

                            $this->filesystem->dumpFile(
                                $news->getImageFile()->getRealPath(),
                                $binary->getContent()
                            );
                        }
                    } catch (\Throwable $throwable) {
                        $output->writeln('News Loader Error: ' . $throwable->getMessage());
                        continue;
                    }
                }
            }
        }

        $this->removeAll();

        $this->entityManager->flush();
    }

    private function getUrls(): array
    {
        return [
            'all' => [
                'rss' => 'https://www.luxco.com/feed/',
                'html' => 'https://www.luxco.com/news/',
            ],
            'news' => [
                'rss' => 'https://www.luxco.com/category/news/feed/',
                'html' => 'https://www.luxco.com/category/news/',
            ],
            'releases' => [
                'rss' => 'https://www.luxco.com/category/press-releases/feed/',
                'html' => 'https://www.luxco.com/category/press-releases/',
            ],
        ];
    }

    private function composeNews($item, $type): News
    {
        return (new News())
            ->setTitle($item->title)
            ->setDescription($item->description)
            ->setLongDescription($item->children('content', true)->encoded)
            ->setPubDate(new \DateTime($item->pubDate))
            ->setType($type)
            ->setImageFile($this->getImage($item->link));
    }

    private function getImage(string $url): ?UploadedFile
    {
        $html = file_get_contents($url);

        if ($html) {
            $crawler = (new Crawler($html))
                ->filter('body .post-content > img')
                ->first();
            if (!empty($crawler->attr('src'))) {
                $file = tempnam(sys_get_temp_dir(), basename($crawler->attr('src')));
                file_put_contents($file, file_get_contents($crawler->attr('src')));
                return new UploadedFile(
                    $file,
                    'fixture',
                    null,
                    null,
                    null,
                    true
                );
            }
        }

        return null;
    }

    private function removeAll()
    {
        foreach ($this->repository->findAll() as $news) {
            $this->entityManager->remove($news);
        }
    }
}
