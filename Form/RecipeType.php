<?php

namespace App\Form;

use App\Entity\Brand;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', CKEditorType::class, [
                'config' => ['toolbar' => 'standard'],
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
            ])
            ->add('brands', EntityType::class, [
                'class' => Brand::class,
                'choice_label' => 'name',
                'multiple' => true,
            ])
            ->add('save', SubmitType::class,
                ['label' => $options['data']->getId() ? 'Update Recipe' : 'Create Recipe']);
    }
}

