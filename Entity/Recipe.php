<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Mapping\EntityBase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ApiResource(
 *     attributes={
 *          "normalization_context"={"groups"={"read"}},
 *          "denormalization_context"={},
 *     },
 *     collectionOperations={"get"={"method"="GET"}},
 *     itemOperations={"get"={"method"="GET"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RecipeRepository")
 * @ApiFilter(BooleanFilter::class, properties={"popular"})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact"})
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Recipe extends EntityBase
{
    /**
     * @Groups("read")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("read")
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Groups("read")
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Groups("read")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="recipe_images", fileNameProperty="image")
     * @Assert\File(mimeTypes = {"image/jpeg", "image/pjpeg", "image/png"})
     * @var File
     */
    private $imageFile;

    /**
     * @Groups("read")
     * @ORM\ManyToMany(targetEntity="App\Entity\Brand", fetch="EAGER")
     * @ORM\JoinTable(name="recipe_x_brand",
     *      joinColumns={@ORM\JoinColumn(name="recipe_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="brand_id", referencedColumnName="id")}
     * )
     */
    private $brands;

    /**
     * @Groups("read")
     * @ORM\Column(type="boolean")
     */
    private $popular = false;

    /**
     * Recipe constructor.
     */
    public function __construct()
    {
        $this->brands = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Recipe
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     *
     * @return Recipe
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     *
     * @return Recipe
     */
    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;

        if ($imageFile instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return Recipe
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param Brand $brand
     *
     * @return $this
     */
    public function addBrand(Brand $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
        }

        return $this;
    }

    /**
     * @param Brand $brand
     *
     * @return Recipe
     */
    public function removeBrand(Brand $brand): self
    {
        if ($this->brands->contains($brand)) {
            $this->brands->removeElement($brand);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    /**
     * @return bool|null
     */
    public function isPopular(): ?bool
    {
        return $this->popular;
    }

    /**
     * @param bool $popular
     *
     * @return Recipe
     */
    public function setPopular(bool $popular): self
    {
        $this->popular = $popular;

        return $this;
    }
}
