<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Mapping\EntityBase;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     attributes={
 *          "normalization_context"={"groups"={"read"}},
 *          "denormalization_context"={},
 *          "pagination_enabled"=false
 *     },
 *     collectionOperations={"get"={"method"="GET"}},
 *     itemOperations={"get"={"method"="GET"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class News extends EntityBase
{
    /**
     * @Groups("read")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("read")
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @Groups("read")
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @Groups("read")
     * @ORM\Column(type="text", nullable=false)
     */
    private $longDescription;

    /**
     * @Groups("read")
     * @ORM\Column(name="pub_date", type="datetime", nullable=false)
     */
    private $pubDate;

    /**
     * @Groups("read")
     * @ORM\Column(type="string", nullable=false)
     */
    private $type;

    /**
     * @Groups("read")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="news_images", fileNameProperty="image")
     * @Assert\File(mimeTypes = {"image/jpeg", "image/pjpeg", "image/png"})
     * @var File
     */
    private $imageFile;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTitle();
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return News
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return News
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongDescription(): string
    {
        return $this->longDescription;
    }

    /**
     * @param string $longDescription
     *
     * @return News
     */
    public function setLongDescription(string $longDescription): self
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getPubDate(): DateTime
    {
        return $this->pubDate;
    }

    /**
     * @param DateTime $pubDate
     *
     * @return News
     */
    public function setPubDate(DateTime $pubDate): self
    {
        $this->pubDate = $pubDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return News
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     *
     * @return News
     */
    public function setImageFile(?File $imageFile): self
    {
        $this->imageFile = $imageFile;

        if ($imageFile instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|string $image
     *
     * @return News
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
