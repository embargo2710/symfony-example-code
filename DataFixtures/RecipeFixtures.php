<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class RecipeFixtures
 * @package App\DataFixtures
 */
class RecipeFixtures extends Fixture
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * UserFixtures constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $url = $this->container->getParameter('url.recipes.xml');
        
	$brands = $manager
            ->getRepository(Brand::class)
            ->createQueryBuilder('p', 'p.external_id')
            ->where('p.external_id != :null')
            ->setParameter('null', 'NULL')
            ->getQuery()
            ->getResult();

        $xml = simplexml_load_file($url);

        if (!$xml) {
            echo 'Ошибка при загрузке XML-файла';
            return;
        }

        if ($xml->Recipes) {
            foreach ($xml->Recipes->Recipe as $element) {
                $recipe = (new Recipe())
                    ->setName($element->Name)
                    ->setDescription($element->Text);

                foreach ($element->AttributeOptions->Id as $attributeOption) {
                    $id = (int)$attributeOption;
                    if (isset($brands[$id])) {
                        $recipe->addBrand($brands[$id]);
                    }
                }

                if ($element->Pictures && $element->Pictures->Url) {
                    $file = tempnam(sys_get_temp_dir(), basename($element->Pictures->Url));
                    file_put_contents($file, file_get_contents($element->Pictures->Url));
                    $image = new UploadedFile($file, 'fixture', null, null, null, true);
                    $recipe->setImageFile($image);
                }

                $manager->persist($recipe);
            }
        }

        $manager->flush();
    }
}
